//
//  DIContainer.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation

import Foundation

struct DIContainer {
    let services: Services
}

extension DIContainer {
    struct Services {
        let messagesService: MessagesService
    }
}

extension DIContainer {
    struct Repositories {
        let remote: RemoteRepositories
        let local: LocalRepositories
    }
}


extension DIContainer {
    struct RemoteRepositories {
        let messagesRemoteRepository: MessagesRemoteRepository
    }
    
    struct LocalRepositories {
        let messagesLocalRepository: MessagesLocalRepository
    }
}

