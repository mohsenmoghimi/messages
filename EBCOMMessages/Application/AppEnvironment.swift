//
//  AppEnvironment.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation
import Alamofire

struct AppEnvironment {
    let container: DIContainer
}

extension AppEnvironment {
    static func bootstrap() -> Self {
        let client = configureClient()
        let remote = configureRepositories(client: client)
        let services = configureServices(repository: remote)
        let container = DIContainer(services: services)
        return .init(container: container)
    }
    
    static func configureClient() -> ApiClient {
        return AlamofireApiClient(session: Session())
    }
    
    static func configureServices(repository: DIContainer.Repositories) -> DIContainer.Services {
        .init(
            messagesService: .init(remote: repository.remote.messagesRemoteRepository, local: repository.local.messagesLocalRepository))
    }
    
    private static func configureRepositories(client: ApiClient) -> DIContainer.Repositories {
        .init(
            remote: .init(messagesRemoteRepository: .init(client: client)),
            local: .init(messagesLocalRepository: .init())
        )
    }
}


