//
//  UIColor.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import UIKit

extension UIColor {
    public class var primaryColor: UIColor {
        UIColor(named: "primaryColor")!
    }
    
    public class var secondaryColor: UIColor {
        UIColor(named: "secondaryColor")!
    }
    
    public class var backgroundColor: UIColor {
        UIColor(named: "backgroundColor")!
    }
    
    public class var secondaryBackgroundColor: UIColor {
        UIColor(named: "secondaryBackgroundColor")!
    }
    
    public class var alertColor: UIColor {
        UIColor(named: "alertColor")!
    }
    
    public class var primaryTextColor: UIColor {
        UIColor(named: "primaryTextColor")!
    }
    
    public class var secondaryTextColor: UIColor {
        UIColor(named: "primaryTextColor")!
    }
}
