//
//  SceneDelegate.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    let environment = AppEnvironment.bootstrap()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        setupNavigatonBar()
        if let windowScene = scene as? UIWindowScene {
            DispatchQueue.main.async {
                let window = UIWindow(windowScene: windowScene)
                self.window = window
                let messagesViewController = MessagesViewController.build(container: self.environment.container)
                let navigationController = UINavigationController(rootViewController: messagesViewController)
                navigationController.setNavigationBarHidden(false, animated: false)
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setupNavigatonBar() {
        let backButtonAppearance = UIBarButtonItemAppearance(style: .plain)
        backButtonAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.white]
        let appearance = UINavigationBarAppearance()
        appearance.backButtonAppearance = backButtonAppearance
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .secondaryColor
        appearance.shadowColor = .clear
        appearance.titleTextAttributes = [.foregroundColor: UIColor.primaryTextColor]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.primaryTextColor]
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UIBarButtonItem.appearance().tintColor = .primaryTextColor
    }
}

