//
//  Messages.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

struct Messages : Codable {
    var messages : [Message]?
    
    enum CodingKeys: String, CodingKey {
        case messages = "messages"
    }
}

struct Message : Codable {
    let title : String?
    let description : String?
    let image : String?
    let id : String
    var unread : Bool?
    var isExpanded : Bool? = false
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case image = "image"
        case id = "id"
        case unread = "unread"
        case isExpanded = "isExpanded"
    }
}
