//
//  ClientError.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation

enum ClientError: Error {
    case serverError(message: String?)
    case parser
    case unknown
    case noContent
    case notFound
    case unauthorized
    case badRequest
    case invalidRequest
}

extension ClientError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .parser:
            return NSLocalizedString("Parser error.", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .unknown:
            return NSLocalizedString("Unknown error.", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .noContent:
            return NSLocalizedString("No Content", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .notFound:
            return NSLocalizedString("Not found", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .unauthorized:
            return NSLocalizedString("Unauthorized Access", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .badRequest:
            return NSLocalizedString("Invalid Request Parameters", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        case .serverError(let message):
            return message
        case .invalidRequest:
            return NSLocalizedString("Invalid Request parameters", tableName: nil, bundle: Bundle.main, value: "", comment: "Error")
        }
    }
}

struct ErrorJson: Codable {
    let code: Int?
    let message: String?
    let data: ErrorData?
    
    struct ErrorData : Codable {
        let status : Int?

        enum CodingKeys: String, CodingKey {
            case status = "status"
        }
    }

}
