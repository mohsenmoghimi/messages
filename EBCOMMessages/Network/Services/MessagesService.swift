//
//  MessagesService.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation

struct MessagesService {
    private let remote: MessagesRemoteRepository
    private let local: MessagesLocalRepository
    
    init(remote: MessagesRemoteRepository, local: MessagesLocalRepository) {
        self.remote = remote
        self.local = local
    }
    
    func getMessages() -> Single<Messages> {
        return remote.getMessages().eraseToAnyPublisher()
    }
    
    func delete(messages: [Message]) -> Single<Messages> {
        return local.delete(items: messages).eraseToAnyPublisher()
    }
    
    func read(message: Message) -> Single<Messages> {
        local.read(message: message).eraseToAnyPublisher()
    }
}


