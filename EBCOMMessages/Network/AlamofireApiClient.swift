//
//  AlamofireApiClient.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation
import Alamofire
import Combine

struct AlamofireApiClient: ApiClient {
    private let session: Session
    private let decoder: JSONDecoder
    
    public init(session: Session, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }
    
    func request<T>(_ urlRequest: UrlRequestConvertor) -> Promise<T> where T : Codable {
        Future { promise in
            guard let urlRequest = try? urlRequest.asURLRequest() else {
                promise(.failure(ClientError.invalidRequest))
                return
            }
            
            let request = self.session.request(urlRequest)
            
            #if DEBUG
            print("--------------------------------:REQUEST:--------------------------------")
            debugPrint(urlRequest)
            #endif
            
            request.validate().responseData(emptyResponseCodes: [200, 204, 205]) { response in
                
                #if DEBUG
                print("--------------------------------:Response:--------------------------------")
                debugPrint(response)
                #endif
                
                switch response.result {
                case .success(let value):
                    do {
                        let model = try decoder.decode(T.self, from: value)
                        promise(.success(model))
                    } catch let err {
                        print(err)
                        promise(.failure(.parser))
                    }
                case .failure(let error):
                    promise(.failure(handleError(urlRequest, error: error, data: response.data)))
                }
            }
        }
    }
}


extension AlamofireApiClient {
    func handleError(_ urlRequest: URLRequestConvertible, error: AFError, data: Data?) -> ClientError {
        guard let data = data else {
            return .unknown
        }
        
        if let codable = try? JSONDecoder().decode(ErrorJson.self, from: data) {
            return .serverError(message: codable.message)
        } else {
            return .unknown
        }
    }
}




