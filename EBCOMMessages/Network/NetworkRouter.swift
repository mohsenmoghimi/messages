//
//  NetworkRouter.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Alamofire
import Foundation

public enum RequestMethod: String {
    case get, post, put, patch, trace, delete
}

protocol NetworkRouter: UrlRequestConvertor {
    var baseURLString :String { get }
    var method: RequestMethod { get }
    var path: String { get }
    var headers: [String: String] { get }
    var params: [String: Any] { get }
    var isTokenRequired: Bool { get }
}

extension NetworkRouter {
    var baseURLString: String {
        return "https://run.mocky.io/v3/"
    }
    
    var isTokenRequired: Bool {
        true
    }
    
    // Add Rout method here
    var method: RequestMethod {
        .post
    }
    
    // Set APIs'Rout for each case
    var path: String {
        ""
    }
    
    // Set header here
    var headers: [String: String] {
        ["Content-Type": "application/json; charset=UTF-8"]
    }
    
    // Return each case parameters
    var params: [String: Any] {
        [:]
    }
    
    func asURLRequest() throws -> URLRequest {
        let urlString = self.baseURLString.appending(path).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: urlString)
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.rawValue.uppercased()
        urlRequest.allHTTPHeaderFields = headers
        
        if method == .get {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: params)
        } else {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        }
        
        return urlRequest
    }
}


// TODO: Tools
extension Dictionary {
    var queryString: String {
        var output: String = ""
        for (key,value) in self {
            output +=  "\(key)=\(value)&"
        }
        output = String(output.dropLast())
        return output
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
