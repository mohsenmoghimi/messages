//
//  MessagesLocalRepository.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation
import Combine

struct MessagesLocalRepository {
    private let decoder = JSONDecoder()
    
    func delete(items: [Message]) -> Promise<Messages> {
        Future { promise in
            if let objects = UserDefaults.standard.value(forKey: "user_messages") as? Data,
               var objectsDecoded = try? decoder.decode(Messages.self, from: objects) as Messages,
               var messages = objectsDecoded.messages {
                messages = messages.filter{ !items.map{$0.id}.contains($0.id) }
                objectsDecoded.messages = messages
                return promise(.success(objectsDecoded))
            } else {
                return promise(.failure(.noContent))
            }
        }
    }
    
    func read(message: Message) -> Promise<Messages> {
        Future { promise in
            if let objects = UserDefaults.standard.value(forKey: "user_messages") as? Data,
               var objectsDecoded = try? decoder.decode(Messages.self, from: objects) as Messages,
               var messages = objectsDecoded.messages {
                if let index = messages.firstIndex(where: {$0.id == message.id}) {
                    messages[index].unread = false
                }
                objectsDecoded.messages = messages
                return promise(.success(objectsDecoded))
            } else {
                return promise(.failure(.noContent))
            }
        }
    }
}
