//
//  MessagesRemoteRepository.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation

struct MessagesRemoteRepository {
    let client: ApiClient
    
    init(client: ApiClient) {
        self.client = client
    }
    
    func getMessages() -> Promise<Messages> {
        return client.request(Router.getMessages)
    }
}


extension MessagesRemoteRepository {
    private enum Router: NetworkRouter {
        case getMessages
        
        var method: RequestMethod {
            switch self {
            case .getMessages:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .getMessages:
                return "729e846c-80db-4c52-8765-9a762078bc82"
            }
        }
        
        var params: [String : Any] {
            return [:]
        }
    }
}

