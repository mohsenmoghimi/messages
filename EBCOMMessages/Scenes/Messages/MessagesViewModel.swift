//
//  MessagesViewModel.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import Foundation
import Combine

final class MessagesViewModel {
    let container: DIContainer
    var cancellables = Set<AnyCancellable>()
    var isLoading = CurrentValueSubject<Bool, Never>(false)
    var error = PassthroughSubject<String, Never>()
    var dataSource = CurrentValueSubject<[Message], Never>([])
    var deletingMessages: [Message] = []
    
    init(container: DIContainer) {
        self.container = container
        fetchMessages()
    }
    
    func fetchMessages() {
        container.services.messagesService.getMessages()
            .sinkToResult { [weak self] result in
            switch result {
            case .success(let value):
                self?.saveObjects(objects: value)
                self?.dataSource.send(self?.sortDataSource(messages: value) ?? [])
                self?.isLoading.send(false)
            case .failure(let err):
                self?.isLoading.send(false)
                self?.error.send(err.localizedDescription)
            }
        }.store(in: &cancellables)
    }
    
    func readMessage(message: Message) {
        container.services.messagesService.read(message: message)
            .sinkToResult { [weak self] result in
            switch result {
            case .success(let value):
                self?.saveObjects(objects: value)
                self?.dataSource.send(self?.sortDataSource(messages: value) ?? [])
                self?.isLoading.send(false)
            case .failure(let err):
                self?.isLoading.send(false)
                self?.error.send(err.localizedDescription)
            }
        }.store(in: &cancellables)
    }
    
    func deleteMessages(messages: [Message]) {
        container.services.messagesService.delete(messages: messages)
            .sinkToResult { [weak self] result in
            switch result {
            case .success(let value):
                self?.saveObjects(objects: value)
                self?.dataSource.send(self?.sortDataSource(messages: value) ?? [])
                self?.isLoading.send(false)
            case .failure(let err):
                self?.isLoading.send(false)
                self?.error.send(err.localizedDescription)
            }
        }.store(in: &cancellables)
    }
    
    func sortDataSource(messages: Messages) -> [Message] {
        var unread = messages.messages?.filter{ $0.unread == true }
        unread = unread?.sorted{ $0.id < $1.id }
        var draft = messages.messages?.filter{ $0.unread == false }
        draft = draft?.sorted{ $0.id < $1.id }
        unread?.append(contentsOf: draft ?? [])
        return unread ?? []
    }
    
    func saveObjects(objects: Messages) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(objects){
            UserDefaults.standard.set(encoded, forKey: "user_messages")
            UserDefaults.standard.synchronize()
        }
    }
    
    func changeExpandation(on message: Message) {
        var messages = dataSource.value
        if let index = messages.firstIndex(where: {$0.id == message.id}) {
            if (messages[index].isExpanded ?? false) {
                messages[index].isExpanded = false
                messages[index].unread = false
            } else {
                messages[index].isExpanded = true
            }
            saveObjects(objects: .init(messages: messages))
            dataSource.send(sortDataSource(messages: .init(messages: messages)) )
        }
    }
}
