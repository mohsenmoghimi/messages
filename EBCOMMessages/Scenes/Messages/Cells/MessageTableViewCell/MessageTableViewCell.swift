//
//  MessageTableViewCell.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import UIKit
import SDWebImage

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageStackView: UIStackView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageDescription: UILabel!
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var expandButton: UIButton! {
        didSet {
            expandButton.addTarget(self, action: #selector(expandButtonAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var shareButton: UIButton! {
        didSet {
            shareButton.addTarget(self, action: #selector(shareButtonAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var cardView: UIView!
    
    var shareButtonPressed: (()->Void)?
    var expandButtonPressed: (()->Void)?
    
    @objc private func shareButtonAction() { shareButtonPressed?() }
    @objc private func expandButtonAction() { expandButtonPressed?() }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        messageImage.isHidden = false
    }

    
    func config(model: Message) {
        if let _ = model.image {
            messageImage.sd_setImage(with: URL(string: model.image ?? ""), placeholderImage: UIImage(systemName: "photo.fill"))
        } else {
            messageImage.isHidden = true
        }
        messageTitle.text = model.title ?? ""
        messageDescription.text = model.description ?? ""
        (model.unread ?? false) ? (cardView.backgroundColor = .white) : (cardView.backgroundColor = .secondaryBackgroundColor)
        if model.isExpanded ?? false {
            configReadedLayout()
            expandButton.setImage(UIImage(systemName: "chevron.up.circle"), for: .normal)
        } else {
            configUnreadedLayout()
            expandButton.setImage(UIImage(systemName: "chevron.down.circle"), for: .normal)
        }
    }
    
    private func configReadedLayout() {
        messageStackView.insertArrangedSubview(messageImage, at: 0)
        messageDescription.numberOfLines = 0
        imageHeightConstraint.constant = 100
        imageWidthConstraint.constant = messageStackView.frame.width
        messageStackView.axis = .vertical
        messageStackView.alignment = .trailing
        layoutSubviews()
    }
    
    private func configUnreadedLayout() {
        messageStackView.insertArrangedSubview(messageDescription, at: 0)
        messageDescription.numberOfLines = 1
        imageHeightConstraint.constant = 40
        imageWidthConstraint.constant = 40
        messageStackView.axis = .horizontal
        messageStackView.alignment = .center
        layoutSubviews()
    }
    
}
