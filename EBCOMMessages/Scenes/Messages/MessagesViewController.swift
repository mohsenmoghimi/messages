//
//  MessagesViewController.swift
//  EBCOMMessages
//
//  Created by Mohsen Moghimi on 2/11/1402 AP.
//

import UIKit

class MessagesViewController: UITableViewController {
    var viewModel: MessagesViewModel!
    
    // MARK: - UI Elements
    lazy var cancelEditBarButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "لغو", style: .done, target: self, action: #selector(cancelEditMode))
        return button
    }()
    
    lazy var trashEditBarButton: UIBarButtonItem = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "trash"), for: .normal)
        button.addTarget(self, action: #selector(deleteItems), for: .touchUpInside)
        button.tintColor = .primaryTextColor
        let barButton = UIBarButtonItem(customView: button)
        return barButton
    }()
    
    private lazy var tableViewRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        refreshControl.tintColor = .primaryColor
        return refreshControl
    }()
    
    // MARK: - View lifecyles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl = tableViewRefreshControl
        self.view.backgroundColor = .backgroundColor
        self.tableView.backgroundColor = .backgroundColor
        self.tableView.allowsMultipleSelectionDuringEditing = true
        self.tableView.separatorStyle = .none
        tableView.register(MessageTableViewCell.self)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(activeEditMode))
        self.tableView.addGestureRecognizer(longPress)
        dataSourceSubscription()
        loadingSubscription()
        errorSubscription()
    }
    
    // MARK: - Subscriptions
    
    private func dataSourceSubscription() {
        viewModel.dataSource
            .receive(on: DispatchQueue.main)
            .sink {[weak self] _ in
                self?.refreshControl?.endRefreshing()
                self?.tableView.reloadData()
            }.store(in: &viewModel.cancellables)
    }
    
    private func loadingSubscription() {
        viewModel.isLoading
            .receive(on: DispatchQueue.main)
            .sink {[weak self] isLoading in
                isLoading ? self?.refreshControl?.beginRefreshing() : self?.refreshControl?.endRefreshing()
            }.store(in: &viewModel.cancellables)
    }
    
    private func errorSubscription() {
        viewModel.error
            .receive(on: DispatchQueue.main)
            .sink {[weak self] message in
                self?.showErrorAlert(message)
            }.store(in: &viewModel.cancellables)
    }
    
    // MARK: - Methods
    
    @objc private func reloadList() {
        viewModel.fetchMessages()
    }
    
    @objc private func activeEditMode() {
        tableView.setEditing(true, animated: true)
        self.navigationItem.rightBarButtonItem = cancelEditBarButton
        self.navigationItem.leftBarButtonItem = trashEditBarButton
    }
    
    @objc private func cancelEditMode() {
        tableView.setEditing(false, animated: true)
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
    }
    
    @objc private func deleteItems() {
        viewModel.deleteMessages(messages: viewModel.deletingMessages)
        cancelEditMode()
    }
    
    func showErrorAlert(_ message: String, handler: ((UIAlertAction)->Void)? = nil) {
        let alert = UIAlertController(title: "خطا", message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "خب!", style: .cancel, handler: handler))
        present(alert, animated: true, completion: nil)
    }
    
    func share(text: String) {
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        present(activityViewController, animated: true, completion: nil)
    }
}

// MARK: - Table view data source

extension MessagesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MessageTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let item = viewModel.dataSource.value[indexPath.row]
        cell.config(model: item)
        cell.shareButtonPressed = { [weak self] in
            if let text = item.description {
                self?.share(text: text)
            }
        }
        cell.expandButtonPressed = { [weak self] in
            self?.viewModel.changeExpandation(on: item)
            self?.tableView.beginUpdates()
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            self?.tableView.endUpdates()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = viewModel.dataSource.value[indexPath.row]
        viewModel.deletingMessages.append(message)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let message = viewModel.dataSource.value[indexPath.row]
        viewModel.deletingMessages = viewModel.deletingMessages.filter{ $0.id != message.id }
    }
}

extension MessagesViewController {
    public static func build(container: DIContainer) -> MessagesViewController {
        let viewController = MessagesViewController()
        let viewModel = MessagesViewModel(container: container)
        viewController.viewModel = viewModel
        return viewController
    }
}
